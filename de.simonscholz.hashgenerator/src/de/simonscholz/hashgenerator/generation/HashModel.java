/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation;

import java.io.File;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class HashModel {
	private SimpleObjectProperty<File> file;

	private SimpleStringProperty md5;

	private SimpleStringProperty sha1;

	public HashModel(File file, String md5, String sha1) {
		this.file = new SimpleObjectProperty<File>(file);
		this.md5 = new SimpleStringProperty(md5);
		this.sha1 = new SimpleStringProperty(sha1);
	}

	public File getFile() {
		return file.get();
	}

	public void setFile(File fileName) {
		this.file.set(fileName);
	}

	public String getMd5() {
		return md5.get();
	}

	public void setMd5(String md5) {
		this.md5.set(md5);
	}

	public String getSha1() {
		return sha1.get();
	}

	public void setSha1(String sha1) {
		this.sha1.set(sha1);
	}
}
