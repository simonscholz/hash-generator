/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation.thread;

import java.io.File;
import java.util.Collection;

import javafx.concurrent.Task;
import de.simonscholz.hashgenerator.generation.HashGenerator;
import de.simonscholz.hashgenerator.generation.HashModel;

public class HashGenerationTask extends Task<Collection<HashModel>> implements
		IProgressUpdater {

	private HashGenerator hashGeneration = new HashGenerator();
	private Collection<File> files;

	public HashGenerationTask(Collection<File> files) {
		this.files = files;
	}

	@Override
	protected Collection<HashModel> call() throws Exception {
		int size200Percent = files.size() * 2;
		updateProgress(0, size200Percent);
		Collection<HashModel> hashModels = hashGeneration.getHashModels(files,
				this);

		return hashModels;
	}

	@Override
	public void updateProgessDelegate(double workDone, double workMax) {
		updateProgress(workDone, workMax);
	}

}
