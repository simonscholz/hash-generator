/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation.thread;

import java.util.Collection;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import de.simonscholz.hashgenerator.generation.HashGenerator;
import de.simonscholz.hashgenerator.generation.HashModel;

public class HashGenerationFileService extends Service<Boolean> {

	private HashGenerator hashGeneration = new HashGenerator();
	private Collection<HashModel> hashModels;
	private String filePattern;

	public HashGenerationFileService(Collection<HashModel> hashModels,
			String filePattern) {
		this.hashModels = hashModels;
		this.filePattern = filePattern;
	}

	@Override
	protected Task<Boolean> createTask() {
		return new Task<Boolean>() {

			@Override
			protected Boolean call() throws Exception {

				try {
					hashGeneration.generateHashFiles(hashModels, filePattern);
				} catch (Exception e) {
					return Boolean.FALSE;
				}

				return Boolean.TRUE;
			}
		};
	}

}
