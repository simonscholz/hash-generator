/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation.thread;

import java.io.File;
import java.util.Collection;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import de.simonscholz.hashgenerator.generation.HashModel;

public class HashGenerationService extends Service<Collection<HashModel>> {

	private Collection<File> files;

	public HashGenerationService(Collection<File> files) {
		this.files = files;
	}

	@Override
	protected Task<Collection<HashModel>> createTask() {
		return new HashGenerationTask(files);
	}
}