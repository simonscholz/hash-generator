/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import de.simonscholz.hashgenerator.generation.hashalgorithm.HashAlgorithm;
import de.simonscholz.hashgenerator.generation.hashalgorithm.MD5Algorithm;
import de.simonscholz.hashgenerator.generation.hashalgorithm.SHA1Algorithm;
import de.simonscholz.hashgenerator.generation.thread.IProgressUpdater;

/**
 * This class is used to generate MD5 and SHA1 values from files and provides a
 * collection {@link HashModel} objects.
 * 
 * @author Simon Scholz
 *
 */
public class HashGenerator {

	private Pattern variablesPattern = Pattern.compile("\\{(.*?)\\}"); //$NON-NLS-1$

	public String getMd5Hash(File file) throws FileNotFoundException, Exception {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		return calculateHash(md5, new FileInputStream(file));
	}

	public Collection<HashModel> getHashModels(Collection<File> files,
			IProgressUpdater progressUpdater) throws FileNotFoundException,
			Exception {
		AtomicInteger atomicInteger = new AtomicInteger();
		int size200Percent = files.size() * 2;
		List<HashModel> hashModels = files
				.parallelStream()
				.filter(f -> f.exists() && !(f.isDirectory()))
				.map(file -> {
					try {
						String md5Hash = getMd5Hash(file);
						progressUpdater.updateProgessDelegate(
								atomicInteger.incrementAndGet(), size200Percent);
						String sha1Hash = getSha1Hash(file);
						progressUpdater.updateProgessDelegate(
								atomicInteger.incrementAndGet(), size200Percent);
						return new HashModel(file, md5Hash, sha1Hash);
					} catch (Exception e) {
						// throw the Exception within stream execution
						throw new RuntimeException(e);
					}
				}).collect(Collectors.toList());
		return hashModels;
	}

	public void generateHashFiles(Collection<HashModel> hashModels,
			String filePattern) {

		MD5Algorithm md5Algorithm = new MD5Algorithm();
		SHA1Algorithm sha1Algorithm = new SHA1Algorithm();

		hashModels.parallelStream().forEach(hashModel -> {
			writeHashFile(hashModel, md5Algorithm, filePattern);
			writeHashFile(hashModel, sha1Algorithm, filePattern);
		});
	}

	private void writeHashFile(HashModel hashModel,
			HashAlgorithm hashAlgorithm, String filePattern) {
		Matcher matcher = variablesPattern.matcher(filePattern);
		Writer hashFileWriter = null;
		try {
			StringBuffer sb = new StringBuffer();

			while (matcher.find()) {
				if (matcher.groupCount() > 0) {
					String variable = matcher.group(1);
					FileGenerationVariables fileGenerationVariable = null;
					try {
						fileGenerationVariable = FileGenerationVariables
								.valueOf(variable);
					} catch (IllegalArgumentException ex) {
						// ignore, if the variable cannot be found in the
						// FileGenerationVariables enum
					}
					String value = "";
					if (fileGenerationVariable != null) {
						switch (fileGenerationVariable) {
						case AbsolutePath:
							value = hashModel.getFile().getAbsolutePath();
							break;
						case HashAlgorithm:
							value = hashAlgorithm.getName();
							break;
						case FileName:
							value = hashModel.getFile().getName();
							break;
						case Path:
							value = hashModel.getFile().getParent();
							break;
						default:
							break;
						}
					}
					matcher.appendReplacement(sb,
							Matcher.quoteReplacement(value));
				}
			}
			matcher.appendTail(sb);

			hashFileWriter = new BufferedWriter(new FileWriter(sb.toString()));
			hashFileWriter.write(hashAlgorithm.getHashValue(hashModel));
			hashFileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (hashFileWriter != null) {
					hashFileWriter.close();
				}
			} catch (IOException e) {
			}
		}
	}

	public String getSha1Hash(File file) throws FileNotFoundException,
			Exception {
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
		return calculateHash(sha1, new FileInputStream(file));
	}

	public static String calculateHash(MessageDigest algorithm,
			InputStream inputStream) throws Exception {

		BufferedInputStream bis = new BufferedInputStream(inputStream);
		DigestInputStream dis = new DigestInputStream(bis, algorithm);
		try {

			// read the file and update the hash calculation
			while (dis.read() != -1)
				;

			// get the hash value as byte array
			byte[] hash = algorithm.digest();

			return byteArray2Hex(hash);
		} catch (Exception e) {
			throw e;
		} finally {
			dis.close();
		}
	}

	private static String byteArray2Hex(byte[] hash) {
		Formatter formatter = new Formatter();
		try {
			for (byte b : hash) {
				formatter.format("%02x", b);
			}
			return formatter.toString();
		} finally {
			formatter.close();
		}
	}
}
