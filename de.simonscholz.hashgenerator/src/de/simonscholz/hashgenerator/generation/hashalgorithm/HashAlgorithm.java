/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation.hashalgorithm;

import de.simonscholz.hashgenerator.generation.HashModel;

/**
 * This is a container used to get a certain {@link HashAlgorithm}.
 * 
 * @see MD5Algorithm
 * @see SHA1Algorithm
 *
 * @author Simon Scholz
 *
 */
public interface HashAlgorithm {

	/**
	 * Get the name of the hash algorithm.
	 * 
	 * @return name of the hash algorithm in small letters
	 */
	public String getName();

	/**
	 * Get the value of the hash for a certain {@link HashAlgorithm} from a
	 * generated {@link HashModel}.
	 * 
	 * @param hashModel
	 *            {@link HashModel}
	 * @return the value of the hash implementation
	 */
	public String getHashValue(HashModel hashModel);
}
