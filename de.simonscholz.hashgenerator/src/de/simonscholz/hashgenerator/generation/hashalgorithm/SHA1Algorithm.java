/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.generation.hashalgorithm;

import de.simonscholz.hashgenerator.generation.HashModel;

/**
 * Information container of the SHA1 hash algorithm.
 * 
 * @author Simon Scholz
 *
 */
public class SHA1Algorithm implements HashAlgorithm {

	@Override
	public String getName() {
		return "sha1";
	}

	@Override
	public String getHashValue(HashModel hashModel) {
		return hashModel.getSha1();
	}

}
