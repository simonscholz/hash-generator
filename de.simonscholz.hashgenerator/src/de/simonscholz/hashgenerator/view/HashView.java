/*
 * Copyright 2014 Simon Scholz
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.simonscholz.hashgenerator.view;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.application.Application;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import de.simonscholz.hashgenerator.generation.FileGenerationVariables;
import de.simonscholz.hashgenerator.generation.HashModel;
import de.simonscholz.hashgenerator.generation.thread.HashGenerationFileService;
import de.simonscholz.hashgenerator.generation.thread.HashGenerationService;

public class HashView extends Application {

	private TableView<HashModel> table;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Hash Generator by Simon Scholz");
		stage.setWidth(850);
		stage.setHeight(450);

		Label label = new Label("Hash Files");
		label.setFont(new Font("Arial", 20));

		ProgressBar progressBar = new ProgressBar(0);
		progressBar.setMaxWidth(Double.MAX_VALUE);

		table = new TableView<HashModel>();

		table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		table.setEditable(true);
		table.setMaxWidth(Double.MAX_VALUE);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.setPlaceholder(new Text(
				"Drop files onto this table in order to generate the hashes"));

		final ObservableList<HashModel> data = FXCollections
				.observableArrayList();

		table.setOnDragOver(event -> {
			/* data is dragged over the target */
			/*
			 * accept it only if it is not dragged from the same node and if it
			 * has a file data
			 */
			if (event.getGestureSource() != event.getSource()
					&& event.getDragboard().hasFiles()) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.LINK);
			}

			event.consume();
		});

		table.setOnDragDropped(event -> {
			Dragboard db = event.getDragboard();
			boolean success = false;
			if (db.hasFiles()) {
				Object source = event.getSource();
				if (source instanceof TableView) {
					List<File> files = db.getFiles();
					try {
						HashGenerationService hashGenerationService = new HashGenerationService(
								files);
						hashGenerationService.start();

						progressBar.progressProperty().bind(
								hashGenerationService.progressProperty());

						hashGenerationService.setOnSucceeded(e -> data
								.addAll((Collection<HashModel>) e.getSource()
										.getValue()));

					} catch (Exception e) {
						e.printStackTrace();
					}
					success = true;
				}
			}

			event.setDropCompleted(success);

			event.consume();
		});

		TableColumn<HashModel, String> fileNameCol = new TableColumn<HashModel, String>(
				"File");
		fileNameCol.setCellValueFactory(param -> new ReadOnlyStringWrapper(
				param.getValue().getFile().getName()));
		fileNameCol
				.setCellFactory(arg -> new TextFieldTableCell<HashModel, String>() {
					@Override
					public void updateItem(String string, boolean isEmpty) {
						super.updateItem(string, isEmpty);
						if (!isEmpty) {
							ObservableList<HashModel> items = getTableView()
									.getItems();

							if (items != null && getTableRow() != null) {
								HashModel model = items.get(getTableRow()
										.getIndex());
								Tooltip tip = new Tooltip(model.getFile()
										.getAbsolutePath());
								setTooltip(tip);
							}
						}
					}
				});
		TableColumn<HashModel, String> hashValueColumn = new TableColumn<HashModel, String>(
				"Hash values");
		TableColumn<HashModel, String> md5Col = new TableColumn<HashModel, String>(
				"MD5");
		md5Col.setCellValueFactory(new PropertyValueFactory<HashModel, String>(
				"md5"));
		md5Col.setCellFactory(arg -> new TextFieldTableCell<HashModel, String>() {
			@Override
			public void updateItem(String string, boolean isEmpty) {
				super.updateItem(string, isEmpty);
				if (!isEmpty) {
					ObservableList<HashModel> items = getTableView().getItems();

					if (items != null && getTableRow() != null) {
						HashModel model = items.get(getTableRow().getIndex());
						Tooltip tip = new Tooltip(model.getMd5());
						setTooltip(tip);
					}
				}
			}
		});
		TableColumn<HashModel, String> sha1Col = new TableColumn<HashModel, String>(
				"Sha 1");
		sha1Col.setCellValueFactory(new PropertyValueFactory<HashModel, String>(
				"sha1"));
		sha1Col.setCellFactory(arg -> new TextFieldTableCell<HashModel, String>() {
			@Override
			public void updateItem(String string, boolean isEmpty) {
				super.updateItem(string, isEmpty);
				if (!isEmpty) {
					ObservableList<HashModel> items = getTableView().getItems();

					if (items != null && getTableRow() != null) {
						HashModel model = items.get(getTableRow().getIndex());
						Tooltip tip = new Tooltip(model.getSha1());
						setTooltip(tip);
					}
				}
			}
		});
		hashValueColumn.getColumns().addAll(md5Col, sha1Col);

		table.getColumns().addAll(fileNameCol, hashValueColumn);

		table.setItems(data);

		VBox tableVBox = new VBox();
		tableVBox.setSpacing(5.0);
		tableVBox.setPadding(new Insets(10, 10, 10, 10));
		tableVBox.getChildren().addAll(label, table, progressBar);
		tableVBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		HBox configHBox = new HBox();
		Label generatedFileNameLabel = new Label("Generated Filenames");
		TextField filePatternText = new TextField(
				"{AbsolutePath}.{HashAlgorithm}");
		filePatternText.setPrefWidth(200.0);
		ComboBox<String> variablesBox = new ComboBox<String>();
		ObservableList<String> stringVariables = FXCollections
				.observableArrayList(Stream
						.of(FileGenerationVariables.values())
						.map(t -> "{" + t.name() + "}")
						.collect(Collectors.toList()));
		variablesBox.setPromptText("Variables");
		variablesBox.setMaxWidth(95);
		variablesBox.setItems(stringVariables);
		variablesBox.setTooltip(new Tooltip("Possible Variables"));
		variablesBox.valueProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				if (null != newValue) {
					int caretPosition = filePatternText.getCaretPosition();
					filePatternText.insertText(caretPosition, newValue);
				}
				variablesBox.getSelectionModel().clearSelection();
			}
		});

		Button generateFilesButton = new Button("Generate files");
		generateFilesButton
				.setOnAction(event -> {
					ObservableList<HashModel> allTableItems = table.getItems();
					HashGenerationFileService hashGenerationFileService = new HashGenerationFileService(
							allTableItems, filePatternText.getText());
					hashGenerationFileService.start();
				});
		Button generateSelectedFilesButton = new Button(
				"Generate selected files");
		generateSelectedFilesButton
				.setOnAction(event -> {
					ObservableList<HashModel> selectedItems = table
							.getSelectionModel().getSelectedItems();

					HashGenerationFileService hashGenerationFileService = new HashGenerationFileService(
							selectedItems, filePatternText.getText());
					hashGenerationFileService.start();

				});
		configHBox.setSpacing(5);
		configHBox.getChildren().addAll(generatedFileNameLabel,
				filePatternText, variablesBox, generateFilesButton,
				generateSelectedFilesButton);
		configHBox.setPadding(new Insets(0, 10, 10, 10));

		BorderPane borderPane = new BorderPane();
		borderPane.setCenter(tableVBox);
		borderPane.setBottom(configHBox);

		Scene scene = new Scene(borderPane);

		stage.setScene(scene);
		stage.show();
	}
}
